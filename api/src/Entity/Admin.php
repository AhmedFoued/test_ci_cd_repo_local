<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity
 */
class Admin extends Account
{

      
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $mobile;

    public function getMobile(): ?string
    {
        return $this->mobile;
    }

    public function setMobile(string $mobile): self
    {
        $this->mobile = $mobile;

        return $this;
    }

        
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $mobile2;

    public function getMobile2(): ?string
    {
        return $this->mobile2;
    }

    public function setMobile2(string $mobile2): self
    {
        $this->mobile2 = $mobile2;

        return $this;
    }


    public function __construct()
    {
        parent::__construct();
     
    
       
    }
            /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles = array('ROLE_ADMIN');

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }
}
